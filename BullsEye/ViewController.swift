//
//  ViewController.swift
//  BullsEye
//
//  Created by Hui Chih Wang on 2020/12/2.
//

import UIKit

class ViewController: UIViewController {
    

    
    private var targetValue: Int = 0
    private var round: Int = 0
    
    private var currentScore: Int {
        abs(targetValue - currentValue)
    }
    
    private var currentValue: Int {
        get {
        Int(slider.value.rounded())
        }
        
        set {
            slider.setValue(Float(newValue), animated: false)
        }
    }
    
    private var totalScore: Int = 0
    
    private var isGoodTargeted: Bool {
        currentScore < 5
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        startNewGame()
        
        let thumbImageNormal = #imageLiteral(resourceName: "SliderThumb-Normal")
        slider.setThumbImage(thumbImageNormal, for: .normal)
        
        let thumbImageHighlighted = #imageLiteral(resourceName: "SliderThumb-Highlighted")
        slider.setThumbImage(thumbImageHighlighted, for: .highlighted)
        
        let insets = UIEdgeInsets(top: 0, left: 14, bottom: 0, right: 14)
        
        let trackLeftImage = #imageLiteral(resourceName: "SliderTrackLeft")
        let trackLeftResizable = trackLeftImage.resizableImage(withCapInsets: insets)
        
        slider.setMinimumTrackImage(trackLeftImage, for: .normal)
        
        let trackRightImage = #imageLiteral(resourceName: "SliderTrackLeft")
        let trackRightResizable = trackRightImage.resizableImage(withCapInsets: insets)
        
        slider.setMaximumTrackImage(trackRightImage, for: .normal)
    }
    
    private func startNewGame() {
        totalScore = 0
        scoreView.text = "Score: \(totalScore)"
        
        round = 0
        startNewRound()
    }
    
    private func startNewRound() {
        round += 1
        targetValue = Int.random(in: 1...100)
        currentValue = 50
        
        targetView.text = "Target: \(targetValue)"
        roundView.text = "Round: \(round)"
    }
    
    private func updateScore() {
        totalScore += currentScore
        scoreView.text = "Score: \(totalScore)"
        print(totalScore)
    }

    @IBOutlet weak var targetView: UITextView!
    
    @IBOutlet weak var scoreView: UITextView!
    
    @IBOutlet weak var roundView: UITextView!
    
    @IBOutlet weak var slider: UISlider!
    
    @IBAction func slideMoved(_ sender: UISlider) {
        currentValue = Int(sender.value.rounded())
    }
    
    @IBAction func showAlert(_ sender: UIButton) {
        let message =  "You scored \(currentScore) points"
        let title = isGoodTargeted ? "Good target" : "Too far from target"
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: "Next Round", style: .default) { _ in
            self.updateScore()
            self.startNewRound()
        }
        
        alert.addAction(action)
        present(alert, animated: true)
    }
    
    @IBAction func resetGame(_ sender: UIButton) {
        startNewGame()
    }
    
}

